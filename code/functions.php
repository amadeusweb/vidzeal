<?php
read_contents(SITEPATH . '/data/content.tsv');

am_var('sections', ['blog']);

function before_render() {
	foreach (am_var('sections') as $slug) {
		$path = am_var('path') . '/content/' . $slug . '/';
		$extension = '.txt';
		$file = $path . am_var('node') . $extension;
		if (file_exists($file)) {
			am_var('fol', $path);
			am_var('section', $slug);
			am_var('file', $file);
			break;
		} else if (file_exists($file = $path . am_var('node') . '.php')) {
			am_var('file', $file);
			break;
		}
	}
}

function did_render_page() {
	if ($section = am_var('section')) {
		renderFile(am_var('file'));
		return true;
	} else if (am_var('file')) {
		disk_include_once(am_var('file'));
		return true;
	}

	return false;
}

function before_file() {
	if (am_var('embed')) return;
	if (am_var('node') != 'index')
		echo '<div style="padding-top: 30px;" class="container" data-aos="fade-up"><h1>' . humanize(am_var('node')) . '</h1><hr />';
}

function after_file() {
	if (am_var('embed')) return;
	if (am_var('node') != 'index')
		echo '</div>';
}

function site_humanize($txt, $field = 'title') {
	$pages = [
		'vidzis story' => 'Vidzi\'s Story',
	];

	if (array_key_exists($key = strtolower($txt), $pages))
		return $pages[$key];

	return $txt;
}

function read_contents($file, $return = false) {
	$cols = 'object';
	$tsv = tsv_to_array(file_get_contents($file), $cols);
	if (!$return) global $content;
	else $content = [];
	if (!$content) $content = [];
	foreach ($tsv as $row) {
		$content[$row[$cols->key]] = $row[$cols->content];
	}
	if ($return) return $content;
}

function content($key, $echo = 1) {
	global $content;
	$result = !isset($_GET['keys']) && isset($content[$key]) ? $content[$key] : 'Missing Content for: ' . $key;
	if ($echo) echo $result; else return $result;
}

function has_content($key) {
	global $content;
	return isset($content[$key]);
}
