<?php
$notOnHome = am_var('node') == 'index' ? '' : ' customjs';
$notHref = am_var('node') == 'index' ? '' : '-xc';
?>

<li class="menu-item"><a class="menu-link<?php echo $notOnHome; ?>" href="<?php echo am_var('url');?>#slider" data-href<?php echo $notHref; ?>="#slider"><div>Home</div></a></li>
<li class="menu-item"><a class="menu-link<?php echo $notOnHome; ?>" href="<?php echo am_var('url');?>#content" data-href<?php echo $notHref; ?>="#content"><div>What We Offer</div></a></li>
<li class="menu-item"><a class="menu-link<?php echo $notOnHome; ?>" href="<?php echo am_var('url');?>#about" data-href<?php echo $notHref; ?>="#about"><div>About</div></a></li>
<li class="menu-item"><a class="menu-link<?php echo $notOnHome; ?>" href="<?php echo am_var('url');?>#video" data-href<?php echo $notHref; ?>="#video"><div>Video</div></a></li>
<li class="menu-item"><a class="menu-link customjs" href="<?php echo am_var('url');?>creations/"><div>2023 Catalogue</div></a></li>
<li><hr /></li>
<li>READ MORE</li>
<?php menu('/content/blog/', ['no-ul' => true, 'li-class' => 'menu-item', 'a-class' => 'menu-link']); ?>
