			<div class="container clearfix" style="padding: 20px;">

				<!-- Footer Widgets
				============================================= -->
				<div class="footer-widgets-wrap clearfix">

					<div class="row justify-content-between">
						<div class="col-md-5">
							<img src="<?php echo am_var('url'); ?>logo-<?php echo am_var('safeName') ?>@2x.png" alt="Image" class="img-fluid" />
							<p class="font-weight-normal"><?php content('Footer Below Logo'); ?></p>
						</div>

						<div class="col-md-5 mt-5 mt-md-4">

							<div id="q-contact" class="widget quick-contact-widget form-widget clearfix">
								<h4 class="nott ls0">Contact</h4>
								<a class="button button-circle button-large btn-block font-weight-normal mt-2" href="https://wa.me/<?php content('Phone'); ?>" target="_blank">Contact on WhatsApp</a>
								<div class="line line-sm border-dark"></div>
								<div class="row justify-content-between clearfix">

									<div class="col-7">
										<span class="<?php echo uses('light-theme') ? '' : 'text-white'; ?>-50 d-block mb-1 font-secondary">Address:</span>
										<address class="h6 mb-3 color3 font-primary" style="line-height: 1.5">
											<?php content('Address'); ?>
										</address>
									</div>

									<div class="col-5">
										<div>
											<span class="<?php echo uses('light-theme') ? '' : 'text-white'; ?>-50 d-block mb-1 font-secondary">Call Us:</span>
											<div class="h6 color3 font-primary"><a href="tel://<?php content('Phone'); ?>"><?php content('Phone'); ?></a></div>
										</div>
										<div class="mt-3">
											<span class="<?php echo uses('light-theme') ? '' : 'text-white'; ?>-50 d-block mb-1 font-secondary">Send an Email:</span>
											<div class="h6 color3 font-primary"><a href="tel://<?php content('Email'); ?>"><?php content('Email'); ?></a></div>
										</div>
									</div>
								</div>
								<?php if (false) { ?>
								<div class="form-result"></div>
								<form id="quick-contact-form" name="quick-contact-form" action="include/form.php" method="post" class="quick-contact-form mb-0">
									<div class="form-process"></div>
									<input type="text" class="required sm-form-control input-block-level" id="quick-contact-form-name" name="quick-contact-form-name" value="" placeholder="Full Name" />
									<input type="text" class="required sm-form-control email input-block-level" id="quick-contact-form-email" name="quick-contact-form-email" value="" placeholder="Email Address" />
									<textarea class="required sm-form-control input-block-level short-textarea" id="quick-contact-form-message" name="quick-contact-form-message" rows="6" cols="30" placeholder="Message"></textarea>
									<input type="text" class="d-none" id="quick-contact-form-botcheck" name="quick-contact-form-botcheck" value="" />
									<input type="hidden" name="prefix" value="quick-contact-form-">
									<button type="submit" id="quick-contact-form-submit" name="quick-contact-form-submit" class="button button-circle button-large btn-block font-weight-normal mt-2" value="submit">Send Email</button>
								</form><?php } ?>
							</div>
						</div>

						<div class="col-8 mt-5 center offset-2 d-flex border border-dark p-3 font-weight-light">
							<?php content('Footer Last Para'); ?>
						</div>
					</div>

				</div>

			</div>