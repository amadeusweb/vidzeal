<?php
include_once 'functions.php';

am_var('local', $local = startsWith($_SERVER['HTTP_HOST'], 'localhost'));

bootstrap(array(
	'name' => 'Vidzeal Skincare [Demo Site]',
	'byline' => 'Luxury Hand-crafted Skincare',
	'safeName' => 'vidzeal',

	'version' => [ 'id' => '19', 'date' => '21 Jun 2023' ],

	'upi_link' => 'upi://pay?pa=srividya.srikumar@okicici&amp;pn=Srividya @ VidZeal&amp;cu=INR',

	'social' => [
	],

	'folder' => 'content/',
	'start_year' => '2016',
	'contact_cta_link' => 'tel:+91-9841223313',
	'contact_cta_text' => 'Call Imran',
	'email' => 'imran@amadeusweb.com',

	'theme' => 'cv-beauty',
	'uses' => 'services1, light-theme', //TODO: reenable
	'styles' => ['styles', 'mozzacart', 'mozzamove'],
	'scripts' => ['mozzacart', 'mozzamove', 'ingredients.data', 'ingredients'],

	'url' => $local ? 'http://localhost/showcase/vidzeal/' : 'https://showcase.amadeusweb.com/vidzeal/',
	'path' => SITEPATH,
	'no-local-stats' => true,
));

render();
?>

