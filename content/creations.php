    <!-- ======= Portfolio Section ======= -->
    <section style="margin-top: 40px" id="portfolio" class="portfolio">
      <div class="container" data-aos="fade-up">
        <?php include_once "assets/mozzamove.html"; ?>
        <div class="section-title">
          <h2 style="margin-right: 100px">Vidzeal Catalogue 2023</h2>
          <p class="orders-byline">From dry skin to pigmentation to eczema to foot pain to body aches to PMS, try our vitalizing and rejuvenating bodybutters, serums, balms and bath salts.</p>
        </div>

<?php
$cols = 'object';
$products = tsv_to_array(file_get_contents(SITEPATH . '/data/catalogue.tsv'), $cols);
$categories = [];
foreach ($products as $p) if ($p[$cols->Category]) $categories[$p[$cols->Category]] = strtolower(str_replace(' ', '-', $p[$cols->Category]));
?>
        <div>
            <ul id="portfolio-filters">
              <li data-filter="all" class="filter-active">All</li>
              <li data-filter=".filter-cart" class="filter-cart">Cart <span class="cart-count"></span></li><?php foreach ($categories as $cat => $slug) {?>
              <li data-filter=".filter-<?php echo $slug; ?>"><?php echo $cat; ?></li><?php } ?>
            </ul>
        </div>

        <div id="mozza-search">
          <input class="search-text" type="text" placeholder="Search" />
          <label><input type="radio" name="type" value="name" />Name</label>
          <label><input type="radio" name="type" value="text" checked />Text</label>
          <!-- <label><input type="radio" name="type" value="offer" />Offer</label> -->
        </div>
        
        <div id="mozza-summary">
          <table class="order-items" border="1">
            <thead><tr><th>Name</th><th>Price</th><th>Quantity</th><th>Amount</th></tr><thead>
            <tbody></tbody>
          </table>
          <p style="background-color: #F84637; color: #fff; font-weight: bold; padding: 15px;">THIS SITE DOESNT YET HAVE ECOMMERCE.</p>
          <a href="javascript: void(0);" data-to="<?php echo_if_var('email');?>" data-name="<?php echo_if_var('name');?>" class="send-order-summary-by-email btn btn-primary">Prepare Order Request Email</a>
          <a href="#" style="display: none" target="_blank" id="order-email-link" class="btn btn-primary">Send Mail</a>
          <a href="#" data-upi-url="<?php echo am_var('upi_link'); ?>" class="place-order-upi btn btn-primary" style="display: none">Pay <span class="total"></span> via UPI</a>
          <!--
          <a href="#" class="place-order-whatsapp hide btn btn-secondary">Order via WhatApp for <span class="total"></span></a>
          -->
        </div>

        <div class="row catalogue-container" data-aos="fade-up" data-aos-delay="300">
<?php
$lastCat = false;
foreach ($products as $p) {
if ($p[$cols->Category]) { $cat = $p[$cols->Category]; $slug = $categories[$cat]; }
$product_slug = strtolower(str_replace(' ', '-', str_replace('.', '', $p[$cols->Name])));
$newCat = $lastCat != $cat ? ' first-of-category' : ''; $lastCat = $cat;
echo '<a name="' . $product_slug . '"></a>';
?>
          <div class="col-lg-4 col-md-6 order-item filter-<?php echo $slug . $newCat; ?>" data-product-slug="<?php echo $product_slug; ?>" data-product-price="<?php echo str_replace(',', '', $p[$cols->Price]); ?>">
            <div class="outer portfolio-wrap">
              <h4><span class="product-name"><?php echo $p[$cols->Name]; ?></span></h4>
              <p class="short-desc"><?php echo $p[$cols->ShortDescription]; ?></p>
              <img src="<?php echo am_var('url');?>assets/catalogue/vidzeal-<?php echo $product_slug; ?>.jpg" class="img-fluid" alt="<?php echo $p[$cols->Name]; ?>">
              <p class="category filter-<?php echo $slug; ?>"><?php echo $cat; ?></p>
              <div class="portfolio-info">
                <p class="desc"><?php echo $p[$cols->Description]; ?></p>
                <br>
				<p class="all-ingredients"><b class="all-ingredients">Key Ingredients:</b></p>
				<p class="ingredients"><?php echo $p[$cols->Ingredients]; ?></p>
				<br>
                <table class="price-and-add"><tr>
					<td width="40%"><?php echo $p[$cols->Size]; ?> |<br />Rs <?php echo $p[$cols->Price]; ?>/-</td>
					<td class="add">
						<span class="toggle-cart">add</span>
						<div class="quantity-div"><button>-</button> <button>+</button> <input type="number" class="quantity-value" readonly value="1" /></div>
					</td>
				</tr></table>
              </div>
            </div>
          </div>
<?php } ?>

        </div>

      </div>
    </section><!-- End Portfolio Section -->
