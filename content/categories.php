<!-- https://docs.google.com/presentation/d/1lmYyhYKIEmnvU4RmRXcTLwFYd1WDO4vYeNqf2ZTidS4/edit#slide=id.p -->
  <section style="margin-top: 40px" class="d-flex align-items-center">
    <div class="container position-relative" data-aos="fade-up" data-aos-delay="100">
      <div class="row">
        <div class="col-md-4 col-12">
          <img src="../assets/srividya-white.jpg" class="img-fluid" />
        </div>
        <div class="col-md-8 col-12">
<ul id="categories" class="blurbs" data-height="70vh">
<li><b>Whipped Soaps (Makeup Removers)</b> <em>Gently massages your skin while cleaning up every pore.</em></li>
<li><b>Charcoal Facepack</b> <em>Does wonders for your skin and health.</em></li>
<li><b>Body Scrubs</b> <em>Leaves your skin all tingly and glowing.</em></li>
<li><b>Bath Salts + Mini Spa</b> <em>Take a load off your feet and feel completely relaxed.</em></li>
<li><b>Body Butters (Moisturizers)</b> <em>A soothing effect with scents that invigorate.</em></li>
<li><b>Face Serum</b> <em>Feel pep and rejuvenated and have a smile on your face.</em></li>
<li><b>Soaps + Children's Soaps</b> <em>For a tingly, soft, happy sensation all over your body.</em></li>
<li><b>Lip Scrub</b> <em>Adding that extra sensuousness to your curvy lips.</em></li>
        </div>
      </div>
    </div>
  </section>