My own mind is filled with the Tolkienese characters like Yavanna Kementari and Varda Elentari, so when asked to write about the Mesopotamian and African goddesses Ishtar and Oshun, I thought let me first delve into the magic of mythology.

As the yugas or cycles wear on, we find ourselves going back to the old pagan ways and days when wood sprites and faeries walked in our midst. When healing was a common, collectively held power not just restricted to shamans, sorceresses and adepts.

The Old World pantheon is, am thrilled to say, still alive in spirit and their influence on our little planet is being intuited more and more as we get in touch with our inner selves.

<a href="https://mythology.wikia.org/wiki/Ishtar" target="_blank">Ishtar</a>, the goddess of Fertility, Love, Storms, and War conjures visions of nature, harmony, exuberance and plenty. Makes me want to visit a forest and sup under an Enchanted Tree when the full moon is out. Not alone, but with a group of treasured friends, who take life full on, always pursuing their fantasies with passion and dedication.

<a href-"https://www.vice.com/amp/en_in/article/3kjepv/how-to-invoke-oshun-yoruba-goddess-orisha" target="_blank">Oshun</a>, typically associated with water, purity, fertility, love, and sensuality, brings out a carnal desire in me to find a betrothed to share my life with and take here in fields and glades of evergreen.

Some of us are privileged to have projected ourselves into the land of Fantasia, that mere utterances of names and characters can trigger visions of places like this:

endless the horizon, green and vast
in its tranquility seemingly lost
those of us who see <a href="https://imran.yieldmore.org/acres-wild/" target="_blank">acres wild</a>
filled with the wonder of a child
