jQuery(document).ready(function () {
	$(ingredients).each(function (ix, item) {
		ingredients[item.Name] = item;
	})

	$('p.ingredients').each(setupIngredients);

	function setupIngredients() {
		var p = $(this);
		var items = p.text().split(', ');
		var output = [];
		p.html('--building--');

		$(items).each(function (ix, item) {
			var txt = ingredients[item] ? '<b class="ingredient" title="Used For: ' + ingredients[item]['Used For'] + '">' + item + '</b>'
				: '<span class="missing-ingredient">' + item + '</span>';
			output.push(txt);
		});

		p.html(output.join(', '));
		$('b.ingredient', p).click(showIngredient);
	}

	function showIngredient() {
		var b = $(this);
		if (b.data('shown')) return;
		b.data('shown', true);
		var list = b.closest('.ingredients').next('.ingredients-list');
		list.append('<b>' + b.text() + '</b>: ' + b.attr('title') + '<br />');
	}
});
