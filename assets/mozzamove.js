function itemPlayer(itemSelector, category) {
	var player = this;

	this.reset = function () {
		this.index = 0;
		this.firstRun = true;
	}

	this.move = function(direction) {
		if(player.index > player.items.length - 1) player.index = direction == 'previous' ? player.items.length - 2 : -1

		var prevTop = $(player.items[player.index]).offset().top;
		var sameTop = true;

		while(sameTop) {
			player.index += direction == 'previous' ? -1 : 1;
			if (player.index < 0) player.index = player.items.length - 1;
			else if (player.index > player.items.length - 1) player.index = 0;
			sameTop = !player.firstRun && prevTop == $(player.items[player.index]).offset().top;
		}

		player.firstRun = false;
		player.scrollTo();
	}

	this.scrollTo = function() {
		player.items[player.index].scrollIntoView();
		window.scrollBy({ top: -0, behavior: 'smooth' }); //nav + admin + cat above heading
	}

	this.itemText = function() {
		return $(category ? '.category' : '.product-name', $(player.items[player.index])).text();
	}

	this.items = $(itemSelector);
	this.reset();
}

$(document).ready(function() {
	$.players = {
		product: new itemPlayer('.order-item', false),
		category: new itemPlayer('.first-of-category', true)
	};

	$('.toolbar span, .toolbar-button').click(function(){
		var el = $(this);
	
		if (el.hasClass('icon-toolbar')) {
			el.siblings().toggle();
			if (el.siblings().first().is(':visible')) $('.toolbar span').css('display', 'inline-block');
			return;
		}

		if (el.hasClass('icon-search')) {
			$('#mozza-search .search-text').focus()[0].scrollIntoView();
			return;
		}

		if (el.hasClass('icon-help')) {
			alert('TODO: We will tell you how it works');
			return;
		}

		if (el.hasClass('name')) {
			var isCategory = el.hasClass('category-name');
			var player = isCategory ? $.players.category : $.players.product;
			player.scrollTo();
			return; //click next button?
		}

		var isCategory = el.hasClass('icon-cat-prev') || el.hasClass('icon-cat-next');
		var direction = el.attr('data-direction');

		var player = isCategory ? $.players.category : $.players.product;
		player.move(direction);
		$(isCategory ? '.toolbar .category-name' : '.toolbar .product-name').text(player.itemText());
	});

	$('#gotoTop').on('click', function(){ $.productPlayer.reset(); $.categoryPlayer.reset(); });
	$('.icon-toolbar').trigger('click');
});
