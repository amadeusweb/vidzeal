/**
 * This is proprietary software of AmadeusWeb.com of Imran Ali Namazi.
 * It cannot be reused, distributed or derived without
 * prior written consent after paying a royalty for it.
 *
 * Mozzacart - The Client Side Shopping Cart of Amadeus.
 *
 * No Database - simply doing a hashchange to mark cart items
 * Finally a url will be sent to the store owner via whatsapp
 * and the flow begins.
 */

jQuery(document).ready(function(){
	var cols = ['#A3EACF', '#E9E9FF', '#F14851', '#FAA648', '#80C357', '#21A47F', '#41539E', '#D8519B', '#DABF91', '#8057C3']; //https://24ways.org/2010/calculating-color-contrast/

	//REGION: INITIALIZE

	$('#portfolio-filters li').each(function(ix, el) { 
		$(this).attr('style', 'background-color: ' + cols[ix] + '; color: ' + getContrast50(cols[ix].substr(1)) + ';');

		var cls = $(this).data('filter'), len = 0;
		if (cls != 'all')
			len = $('p' + cls).attr('style', 'background-color: ' + cols[ix] + '; color: ' + getContrast50(cols[ix].substr(1)) + ';').length;

		if (cls != '.filter-cart')
			$(this).text($(this).text() + ' (' + len + ')')
	});

	function getContrast50(hexcolor){ return (parseInt(hexcolor, 16) > 0xffffff/2) ? 'black':'white'; }

	var cartFilter;
	$('#portfolio-filters li').click(filterClick);
	function filterClick() {
		var cls = $(this).data('filter');
		var all = cls == 'all';
		cartFilter = cls == '.filter-cart' ? $(this) : false;
		$('.order-item').each(function() { if (all || $(this).is(cls)) $(this).show(); else $(this).hide(); });
	}

	//REGION: CART ADD / REMOVE + QTY

	$('.toggle-cart').click(toggleCart);
	function toggleCart() {
		var itm = $(this).closest('.order-item');
		itm.toggleClass('filter-cart');
		$(this).text(itm.hasClass('filter-cart') ? 'remove' : 'add');
		if (cartFilter) cartFilter.trigger('click');
		var counted = $('.order-item.filter-cart').length;
		$('#portfolio-filters .cart-count').text(counted ? '(' + counted + ')' : '[empty]');
		if (counted) $('#mozza-summary').show(); else $('#mozza-summary').hide();
		saveToCookie();
	}

	$('.quantity-div button').click(quantityAdjust);
	function quantityAdjust() {
		var divIn = $(this).closest('.quantity-div');
		var txtBox = $('.quantity-value', divIn);
		var quantity = parseInt(txtBox.val());
		var increase = $(this).text() == '+';
		if (quantity == 1 && !increase) return;
		quantity += increase ? 1 : -1;
		txtBox.val(quantity);
		saveToCookie();
	}

	//REGION: SEARCH

	$('#mozza-search input[type=radio]').change(searchTypeChanged);
	function searchTypeChanged() {
		if ($(this).is(':checked')) {
			lastSearchFor = '';
			$('#mozza-search .quantity-value').trigger('change');
		}
	}

	var lastSearchFor = '';
	$('#mozza-search .search-text').on('propertychange change click keyup input paste', mozzaSearch);
	function mozzaSearch() {
		var searchFor = $(this).val().toLowerCase();
		if (lastSearchFor != searchFor) {
			lastSearchFor = searchFor;
			var what = $('#mozza-search input[type=radio]:checked').val();
			$('.order-item').each(function() {
				var match = false, divIn = $(this);
				if ($('.product-name', divIn).text().toLowerCase().indexOf(searchFor) !== -1) match = true;
				else if (what == 'text' && 
				(
					$('.short-desc', divIn).text().toLowerCase().indexOf(searchFor) !== -1
					|| $('.desc', divIn).text().toLowerCase().indexOf(searchFor) !== -1
				)) match = true;
				
				if (match) $(this).show(); else $(this).hide();
			});
		}
	}

	//REGION: LINKS TO SEND

	$('.send-order-summary-by-email').on('click', mozzaSendByEmail);
	function mozzaSendByEmail() {
		var to = $(this).data('to');
		var name = $(this).data('name');
		var subject = 'Order to ' + name + ' of ' + (new Date()).toDateString();
		var body = 'Dear ' + name + ",%0D%0D" +
			'Please inform availability and estimated dispatch date of the following:' + "%0D" +
			location.href + "%0D%0D" +
			'and if the amount ' + $('#mozza-summary .total').text() + ' is final' + "%0D%0D" +
			'I understand that the charges may vary depending on the amount of support you give me on phone counselling / guidance.' + "%0D%0D";
		var link = 'mailto:' + to + '?subject=' + subject + '&body=' + body;
		$('#order-email-link').attr('href', link).show().trigger('click');
	}


	//REGION: HEAVY LIFTING

	function saveToCookie() {
		var items = $('.order-item.filter-cart').map(function(ix, el) { return $(el).data('product-slug') + '=' + $('.quantity-div .quantity-value', $(el)).val(); }).get();
		var total = 0;
		var rows = $('.order-item.filter-cart').map(function(ix, el) { 
			var quantity = $('.quantity-div .quantity-value', $(el)).val(), price = $(el).data('product-price'), amount = parseInt(quantity) * parseInt(price);
			total += amount;
			return '  <tr><td>' + $('.product-name', $(el)).text()  + '</td><td>' + price + '</td><td>' + quantity + '</td><td>' + amount + '</td></tr>';
		}).get().join("\r\n");

		//total = Math.round(total/100); //TODO: remove after testing
		$('#mozza-summary .order-items tbody').html(rows)
		$('#mozza-summary .total').text('Rs ' + total + '/-');

		var orderUpi = $('#mozza-summary .place-order-upi');
		orderUpi.attr('href', orderUpi.data('upi-url') + '&am=' + total); //https://stackoverflow.com/questions/67681952/upi-payment-showing-error-for-security-reasons-you-are-not-allowed-to-send-mon

		var cookie = location.hash = '#mozza/' + items.join('/');
	}

	$('#mozza-summary .place-order-whatsapp').click(placeOrder);
	function placeOrder(e) {
		alert('//TODO: Send the url to Vidzi on WhatsApp');
		e.preventDefault();
	}
});
