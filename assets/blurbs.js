$(document).ready(function(){
    Halloween.interval = 700;
    Halloween.offset = { X: 150, Y: 150 };

    $('.blurbs').each(function() {
        var ghostImages = $('li', $(this)).map(function(){ return $(this).html(); });
        var id = $(this).attr('id');
        $(this).hide();
        $('<div class="blurbs-container" id="' + id + '-div" />').height($(this).data('height')).css({position: 'relative'}).insertAfter('#' + id); 
        Halloween.ghostOutput(ghostImages, id);
    });
});
